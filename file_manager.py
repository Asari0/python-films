# Your code goes here.
import json


def count(filename):
    """Ouvre le fichier JSON, et compte le nombre d'éléments présents dans la structure."""
    data = []
    with open(filename) as file:
        data = json.load(file)
        length = len(data)
    return length


def written_by(filename, name):
    """Ouvre le fichier JSON, et cherche les enregistrements écrits par Vince Gilligan"""
    data = []
    with open(filename) as file:
        data = json.load(file)
    list_film = []
    for i in range(len(data)):
        if 'Writer' not in data[i]:
            i += 1
        elif data[i]["Writer"] == "N/A":
            i += 1
        elif name in data[i]["Writer"]:
            list_film.append(data[i])
    return list_film
